# Prestwood

Prestwood is a daemon responsible for maintaining the mount points list

## Overview

This service maintains mount volumes list. It sends notification whenever
new volume is added or already mounted point is removed This service is
responsible for updating canterbury media manager service with upnp status.

## Contact

[Mail the maintainers](mailto:maintainers@lists.apertis.org)
